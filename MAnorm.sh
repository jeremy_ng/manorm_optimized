## remove dump file printing to reduce IO on phoebe
## remove hard coding of args3 and 4 in script
## replace sort commands with bedtools sort -i 

if [ $# -ne 6 ]
then
  echo "Usage: `basename $0` peak1.bed peak2.bed read1.bed read2.bed bp_shift_1 bp_shift_2"
  exit 
fi

echo "StepI: clean input"
sed 's/\s$//g' $1 | awk 'BEGIN {OFS="\t"}
     {if ($1~/chr/ && $1 !="chrM" && $1 !~/random/ && $3>$2 && $2>0 && $3>0)
          print $1,$2,$3>"peak1.bed"}'
sed 's/\s$//g' $2 | awk 'BEGIN {OFS="\t"}
     {if ($$1~/chr/ && 1 !="chrM"  && $1 !~/random/ && $3>$2  && $2>0 && $3>0)
          print $1,$2,$3>"peak2.bed"}' 
sed 's/\s$//g' $3 | awk -v var=$5 'BEGIN {OFS="\t"}
     {if ($1~/chr/ && $1 !="chrM" && $4=="+" && $1 !~/random/ && $3>$2  && $2>0 && $3>0)
          print $1,$2+var,$3+var>"read1.bed";
      else if ($1~/chr/  && $1 !="chrM" && $4=="-" && $1 !~/random/ && $3>$2  && $2>var && $3>var)
          print $1,$2-var,$3-var>"read1.bed"}'
sed 's/\s$//g' $4 | awk -v var=$6 'BEGIN {OFS="\t"}
     {if ($1~/chr/ && $1 !="chrM" && $4=="+" && $1 !~/random/ && $3>$2  && $2>0 && $3>0)
          print $1,$2+var,$3+var>"read2.bed";
      else if ($1~/chr/  && $1 !="chrM" && $4=="-" && $1 !~/random/ && $3>$2  && $2>var && $3>var)
          print $1,$2-var,$3-var>"read2.bed"}'


echo "StepII: classify common or unique peaks"
intersectBed -a peak1.bed -b peak2.bed -u |bedtools sort -i > common_peak1.bed
intersectBed -a peak2.bed -b peak1.bed -u |bedtools sort -i > common_peak2.bed
intersectBed -a peak1.bed -b peak2.bed -v |bedtools sort -i > unique_peak1.bed
intersectBed -a peak2.bed -b peak1.bed -v |bedtools sort -i > unique_peak2.bed

#cat common_peak1.bed common_peak2.bed | mergeBed -i - > common_peak.bed
cat common_peak1.bed common_peak2.bed > temp_common_peak.bed
bedtools sort -i temp_common_peak.bed|mergeBed > common_peak.bed

 
echo "StepIII: count peak read"
if [ -f MAnorm.bed ];
then
rm MAnorm.bed
fi
echo "StepIIIA: Calculating coverage"
coverageBed -a $3 -b unique_peak1.bed |bedtools sort -i | awk 'BEGIN {OFS="\t"} {print $1,$2,$3,"unique_peak1" >> "MAnorm.bed"; print $5 > "unique_peak1_count_read1"}'
coverageBed -a $4 -b unique_peak1.bed |bedtools sort -i | awk '{print $4 > "unique_peak1_count_read2"}'
coverageBed -a $3 -b common_peak1.bed |bedtools sort -i | awk 'BEGIN {OFS="\t"} {print $1,$2,$3,"common_peak1" >> "MAnorm.bed";print $5 > "common_peak1_count_read1"}'
coverageBed -a $4 -b common_peak1.bed | bedtools sort -i  | awk '{print $4 > "common_peak1_count_read2"}'
coverageBed -a $3 -b common_peak2.bed | bedtools sort -i  | awk 'BEGIN {OFS="\t"} {print $1,$2,$3,"common_peak2"  >> "MAnorm.bed";print $5 > "common_peak2_count_read1"}'
coverageBed -a $4 -b common_peak2.bed |bedtools sort -i  |  awk '{print $4 > "common_peak2_count_read2"}'
coverageBed -a $3 -b unique_peak2.bed | bedtools sort -i  | awk 'BEGIN {OFS="\t"} {print $1,$2,$3,"unique_peak2">> "MAnorm.bed";print $5 > "unique_peak2_count_read1"}'

cat unique_peak1_count_read1 common_peak1_count_read1 common_peak2_count_read1 unique_peak2_count_read1 > peak_count_read1
cat unique_peak1_count_read2 common_peak1_count_read2 common_peak2_count_read2 unique_peak2_count_read2 > peak_count_read2

if [ -f MAnorm_merge.bed ];
then
rm MAnorm_merge.bed
rm temp_common_peak.bed
# rm *count*
# rm *read1*
# rm *read2*
# rm *peak1*
# rm *peak2*
# rm MAnorm.bed
# rm MAnorm_merge.bed
# rm common_peak.bed
